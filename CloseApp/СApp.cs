﻿using System;
using System.ServiceProcess;
using System.Timers;
using System.IO;
using System.Diagnostics;

namespace CloseApp
{
    public partial class CloseApp : ServiceBase
    {
        Timer t1,t2;
        StreamWriter sw;
        string application;
        public CloseApp()
        {
            try
            {
                InitializeComponent();
                t1 = new Timer();
                t2 = new Timer();
                t1.Elapsed += cycle;
                t2.Elapsed += check;
                t2.Interval = 10000;
            }
            catch (Exception e)
            {
                
            }
        }

        private void check(object sender, ElapsedEventArgs e)
        {
            if(Process.GetProcessesByName(application).GetLength(0)==0)
            {
                sw.WriteLine(DateTime.Now.ToString() + " Процесс " + application + " был завершен иной службой.");
                this.Stop();
            }
        }

        private void cycle(object sender, ElapsedEventArgs e)
        {
            Process.GetProcessesByName(application)[0].Kill();
            sw.WriteLine(DateTime.Now.ToString()+" Процесс "+application+" завершен.");
            this.Stop();
        }

        protected override void OnStart(string[] args)
        {
            try {
                if (args.GetLength(0) > 0)
                {
                    sw = new StreamWriter(args[2], true);
                    sw.WriteLine(DateTime.Now.ToString() + " Служба запущенная.");
                    application = args[0];
                    t1.Interval = Convert.ToInt32(args[1]) * 60000;
                    t1.Start();
                    t2.Start();
                    sw.WriteLine(DateTime.Now.ToString() + " Запланированно завершение процесса " + application + " через " + (t1.Interval / 60000) + " мин.");
                }
            }
            catch(Exception e)
            {
                sw.WriteLine(DateTime.Now.ToString() + " " + e.Message);
            }
        }

        protected override void OnStop()
        {
            try
            {
                sw.WriteLine(DateTime.Now.ToString() + " Служба отключена.");
                t1.Stop();
                t2.Stop();
                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                sw.WriteLine(DateTime.Now.ToString() + " " + e.Message);
            }
        }
    }
}