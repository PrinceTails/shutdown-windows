﻿namespace Shutdown
{
    partial class Main
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.logoffRB = new System.Windows.Forms.RadioButton();
            this.restartRB = new System.Windows.Forms.RadioButton();
            this.shutdownRB = new System.Windows.Forms.RadioButton();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.firstRB = new System.Windows.Forms.RadioButton();
            this.secondRB = new System.Windows.Forms.RadioButton();
            this.hourBox = new System.Windows.Forms.ComboBox();
            this.minuteBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.hardReboot = new System.Windows.Forms.CheckBox();
            this.Acept = new System.Windows.Forms.Button();
            this.Abort = new System.Windows.Forms.Button();
            this.setMinutes = new System.Windows.Forms.NumericUpDown();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.chooseAction = new System.Windows.Forms.TabControl();
            this.shutPage = new System.Windows.Forms.TabPage();
            this.closePage = new System.Windows.Forms.TabPage();
            this.deleteService = new System.Windows.Forms.Button();
            this.notification = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.searchService = new System.Windows.Forms.Button();
            this.refreshList = new System.Windows.Forms.Button();
            this.abortClose = new System.Windows.Forms.Button();
            this.acceptClose = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.closingTimer = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.appList = new System.Windows.Forms.ComboBox();
            this.openLog = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setMinutes)).BeginInit();
            this.statusBar.SuspendLayout();
            this.chooseAction.SuspendLayout();
            this.shutPage.SuspendLayout();
            this.closePage.SuspendLayout();
            this.notification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closingTimer)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.logoffRB);
            this.groupBox1.Controls.Add(this.restartRB);
            this.groupBox1.Controls.Add(this.shutdownRB);
            this.groupBox1.Location = new System.Drawing.Point(6, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 112);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Методы завершения работы";
            // 
            // logoffRB
            // 
            this.logoffRB.AutoSize = true;
            this.logoffRB.Image = global::Shutdown.Properties.Resources.logoff;
            this.logoffRB.Location = new System.Drawing.Point(249, 19);
            this.logoffRB.Name = "logoffRB";
            this.logoffRB.Size = new System.Drawing.Size(90, 81);
            this.logoffRB.TabIndex = 2;
            this.logoffRB.Text = "Разлогинить";
            this.logoffRB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.logoffRB.UseVisualStyleBackColor = true;
            // 
            // restartRB
            // 
            this.restartRB.AutoSize = true;
            this.restartRB.Image = global::Shutdown.Properties.Resources.restart;
            this.restartRB.Location = new System.Drawing.Point(125, 19);
            this.restartRB.Name = "restartRB";
            this.restartRB.Size = new System.Drawing.Size(97, 81);
            this.restartRB.TabIndex = 1;
            this.restartRB.Text = "Перезагрузка";
            this.restartRB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.restartRB.UseVisualStyleBackColor = true;
            // 
            // shutdownRB
            // 
            this.shutdownRB.AutoSize = true;
            this.shutdownRB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.shutdownRB.Checked = true;
            this.shutdownRB.Image = global::Shutdown.Properties.Resources.shutdown;
            this.shutdownRB.Location = new System.Drawing.Point(10, 19);
            this.shutdownRB.Name = "shutdownRB";
            this.shutdownRB.Size = new System.Drawing.Size(89, 81);
            this.shutdownRB.TabIndex = 0;
            this.shutdownRB.TabStop = true;
            this.shutdownRB.Text = "Выключение";
            this.shutdownRB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.shutdownRB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.shutdownRB.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Enabled = false;
            this.dateTimePicker.Location = new System.Drawing.Point(131, 167);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(119, 20);
            this.dateTimePicker.TabIndex = 3;
            this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // firstRB
            // 
            this.firstRB.AutoSize = true;
            this.firstRB.Checked = true;
            this.firstRB.Location = new System.Drawing.Point(12, 138);
            this.firstRB.Name = "firstRB";
            this.firstRB.Size = new System.Drawing.Size(113, 17);
            this.firstRB.TabIndex = 4;
            this.firstRB.TabStop = true;
            this.firstRB.Text = "Выполнить через";
            this.firstRB.UseVisualStyleBackColor = true;
            this.firstRB.CheckedChanged += new System.EventHandler(this.firstRB_CheckedChanged);
            // 
            // secondRB
            // 
            this.secondRB.AutoSize = true;
            this.secondRB.Location = new System.Drawing.Point(12, 167);
            this.secondRB.Name = "secondRB";
            this.secondRB.Size = new System.Drawing.Size(118, 17);
            this.secondRB.TabIndex = 5;
            this.secondRB.Text = "Запланировать на";
            this.secondRB.UseVisualStyleBackColor = true;
            this.secondRB.CheckedChanged += new System.EventHandler(this.secondRB_CheckedChanged);
            // 
            // hourBox
            // 
            this.hourBox.DropDownHeight = 80;
            this.hourBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hourBox.DropDownWidth = 20;
            this.hourBox.Enabled = false;
            this.hourBox.FormattingEnabled = true;
            this.hourBox.IntegralHeight = false;
            this.hourBox.Location = new System.Drawing.Point(261, 166);
            this.hourBox.Name = "hourBox";
            this.hourBox.Size = new System.Drawing.Size(40, 21);
            this.hourBox.TabIndex = 6;
            this.hourBox.SelectedIndexChanged += new System.EventHandler(this.hourBox_SelectedIndexChanged);
            // 
            // minuteBox
            // 
            this.minuteBox.DropDownHeight = 80;
            this.minuteBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.minuteBox.DropDownWidth = 20;
            this.minuteBox.Enabled = false;
            this.minuteBox.FormattingEnabled = true;
            this.minuteBox.IntegralHeight = false;
            this.minuteBox.Location = new System.Drawing.Point(307, 166);
            this.minuteBox.MaxLength = 60;
            this.minuteBox.Name = "minuteBox";
            this.minuteBox.Size = new System.Drawing.Size(40, 21);
            this.minuteBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(195, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "мин.";
            // 
            // hardReboot
            // 
            this.hardReboot.AutoSize = true;
            this.hardReboot.Location = new System.Drawing.Point(12, 194);
            this.hardReboot.Name = "hardReboot";
            this.hardReboot.Size = new System.Drawing.Size(269, 17);
            this.hardReboot.TabIndex = 10;
            this.hardReboot.Text = "Принудительное завершение работы программ";
            this.hardReboot.UseVisualStyleBackColor = true;
            // 
            // Acept
            // 
            this.Acept.Location = new System.Drawing.Point(12, 217);
            this.Acept.Name = "Acept";
            this.Acept.Size = new System.Drawing.Size(120, 25);
            this.Acept.TabIndex = 11;
            this.Acept.Text = "Подтвердить";
            this.Acept.UseVisualStyleBackColor = true;
            this.Acept.Click += new System.EventHandler(this.Acept_Click);
            // 
            // Abort
            // 
            this.Abort.Location = new System.Drawing.Point(227, 217);
            this.Abort.Name = "Abort";
            this.Abort.Size = new System.Drawing.Size(120, 25);
            this.Abort.TabIndex = 12;
            this.Abort.Text = "Отменить";
            this.Abort.UseVisualStyleBackColor = true;
            this.Abort.Click += new System.EventHandler(this.Abort_Click);
            // 
            // setMinutes
            // 
            this.setMinutes.Location = new System.Drawing.Point(131, 138);
            this.setMinutes.Name = "setMinutes";
            this.setMinutes.Size = new System.Drawing.Size(61, 20);
            this.setMinutes.TabIndex = 13;
            this.setMinutes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // statusBar
            // 
            this.statusBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 274);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(361, 22);
            this.statusBar.TabIndex = 14;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(91, 17);
            this.statusLabel.Text = "Готов к работе.";
            // 
            // chooseAction
            // 
            this.chooseAction.Controls.Add(this.shutPage);
            this.chooseAction.Controls.Add(this.closePage);
            this.chooseAction.Location = new System.Drawing.Point(0, 1);
            this.chooseAction.Name = "chooseAction";
            this.chooseAction.SelectedIndex = 0;
            this.chooseAction.Size = new System.Drawing.Size(364, 275);
            this.chooseAction.TabIndex = 16;
            this.chooseAction.SelectedIndexChanged += new System.EventHandler(this.chooseAction_SelectedIndexChanged);
            // 
            // shutPage
            // 
            this.shutPage.Controls.Add(this.firstRB);
            this.shutPage.Controls.Add(this.groupBox1);
            this.shutPage.Controls.Add(this.setMinutes);
            this.shutPage.Controls.Add(this.dateTimePicker);
            this.shutPage.Controls.Add(this.Abort);
            this.shutPage.Controls.Add(this.secondRB);
            this.shutPage.Controls.Add(this.Acept);
            this.shutPage.Controls.Add(this.hourBox);
            this.shutPage.Controls.Add(this.hardReboot);
            this.shutPage.Controls.Add(this.minuteBox);
            this.shutPage.Controls.Add(this.label1);
            this.shutPage.Location = new System.Drawing.Point(4, 22);
            this.shutPage.Name = "shutPage";
            this.shutPage.Padding = new System.Windows.Forms.Padding(3);
            this.shutPage.Size = new System.Drawing.Size(356, 249);
            this.shutPage.TabIndex = 0;
            this.shutPage.Text = "Завершение работы";
            this.shutPage.UseVisualStyleBackColor = true;
            // 
            // closePage
            // 
            this.closePage.Controls.Add(this.openLog);
            this.closePage.Controls.Add(this.deleteService);
            this.closePage.Controls.Add(this.notification);
            this.closePage.Controls.Add(this.searchService);
            this.closePage.Controls.Add(this.refreshList);
            this.closePage.Controls.Add(this.abortClose);
            this.closePage.Controls.Add(this.acceptClose);
            this.closePage.Controls.Add(this.label4);
            this.closePage.Controls.Add(this.label3);
            this.closePage.Controls.Add(this.closingTimer);
            this.closePage.Controls.Add(this.label2);
            this.closePage.Controls.Add(this.appList);
            this.closePage.Location = new System.Drawing.Point(4, 22);
            this.closePage.Name = "closePage";
            this.closePage.Padding = new System.Windows.Forms.Padding(3);
            this.closePage.Size = new System.Drawing.Size(356, 249);
            this.closePage.TabIndex = 1;
            this.closePage.Text = "Закрытие программы";
            this.closePage.UseVisualStyleBackColor = true;
            // 
            // deleteService
            // 
            this.deleteService.Location = new System.Drawing.Point(197, 122);
            this.deleteService.Name = "deleteService";
            this.deleteService.Size = new System.Drawing.Size(150, 23);
            this.deleteService.TabIndex = 18;
            this.deleteService.Text = "Удалить службу";
            this.deleteService.UseVisualStyleBackColor = true;
            this.deleteService.Click += new System.EventHandler(this.deleteService_Click);
            // 
            // notification
            // 
            this.notification.Controls.Add(this.textBox1);
            this.notification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.notification.ForeColor = System.Drawing.Color.Red;
            this.notification.Location = new System.Drawing.Point(12, 152);
            this.notification.Name = "notification";
            this.notification.Size = new System.Drawing.Size(335, 60);
            this.notification.TabIndex = 17;
            this.notification.TabStop = false;
            this.notification.Text = "Внимание";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.ForeColor = System.Drawing.Color.Red;
            this.textBox1.Location = new System.Drawing.Point(7, 21);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(321, 35);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Приложение будет принудительно закрыто.\r\nУбедитесь, что все необходимые изменения" +
    " сохранены.";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // searchService
            // 
            this.searchService.Location = new System.Drawing.Point(12, 122);
            this.searchService.Name = "searchService";
            this.searchService.Size = new System.Drawing.Size(150, 23);
            this.searchService.TabIndex = 16;
            this.searchService.Text = "Поиск службы";
            this.searchService.UseVisualStyleBackColor = true;
            this.searchService.Click += new System.EventHandler(this.searchService_Click);
            // 
            // refreshList
            // 
            this.refreshList.Location = new System.Drawing.Point(12, 93);
            this.refreshList.Name = "refreshList";
            this.refreshList.Size = new System.Drawing.Size(150, 23);
            this.refreshList.TabIndex = 15;
            this.refreshList.Text = "Обновить список приложений";
            this.refreshList.UseVisualStyleBackColor = true;
            this.refreshList.Click += new System.EventHandler(this.refreshList_Click);
            // 
            // abortClose
            // 
            this.abortClose.Location = new System.Drawing.Point(197, 217);
            this.abortClose.Name = "abortClose";
            this.abortClose.Size = new System.Drawing.Size(150, 25);
            this.abortClose.TabIndex = 14;
            this.abortClose.Text = "Отменить";
            this.abortClose.UseVisualStyleBackColor = true;
            this.abortClose.Click += new System.EventHandler(this.abortClose_Click);
            // 
            // acceptClose
            // 
            this.acceptClose.Location = new System.Drawing.Point(12, 217);
            this.acceptClose.Name = "acceptClose";
            this.acceptClose.Size = new System.Drawing.Size(150, 25);
            this.acceptClose.TabIndex = 13;
            this.acceptClose.Text = "Подтвердить";
            this.acceptClose.UseVisualStyleBackColor = true;
            this.acceptClose.Click += new System.EventHandler(this.acceptClose_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "мин.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Закрыть через";
            // 
            // closingTimer
            // 
            this.closingTimer.Location = new System.Drawing.Point(99, 67);
            this.closingTimer.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.closingTimer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.closingTimer.Name = "closingTimer";
            this.closingTimer.Size = new System.Drawing.Size(120, 20);
            this.closingTimer.TabIndex = 2;
            this.closingTimer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Список приложений";
            // 
            // appList
            // 
            this.appList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appList.FormattingEnabled = true;
            this.appList.Location = new System.Drawing.Point(12, 36);
            this.appList.Name = "appList";
            this.appList.Size = new System.Drawing.Size(335, 21);
            this.appList.TabIndex = 0;
            // 
            // openLog
            // 
            this.openLog.Location = new System.Drawing.Point(197, 93);
            this.openLog.Name = "openLog";
            this.openLog.Size = new System.Drawing.Size(150, 23);
            this.openLog.TabIndex = 19;
            this.openLog.Text = "Открыть лог службы";
            this.openLog.UseVisualStyleBackColor = true;
            this.openLog.Click += new System.EventHandler(this.openLog_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 296);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.chooseAction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shutdown Windows";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setMinutes)).EndInit();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.chooseAction.ResumeLayout(false);
            this.shutPage.ResumeLayout(false);
            this.shutPage.PerformLayout();
            this.closePage.ResumeLayout(false);
            this.closePage.PerformLayout();
            this.notification.ResumeLayout(false);
            this.notification.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closingTimer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton logoffRB;
        private System.Windows.Forms.RadioButton restartRB;
        private System.Windows.Forms.RadioButton shutdownRB;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.RadioButton firstRB;
        private System.Windows.Forms.RadioButton secondRB;
        private System.Windows.Forms.ComboBox hourBox;
        private System.Windows.Forms.ComboBox minuteBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox hardReboot;
        private System.Windows.Forms.Button Acept;
        private System.Windows.Forms.Button Abort;
        private System.Windows.Forms.NumericUpDown setMinutes;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.TabControl chooseAction;
        private System.Windows.Forms.TabPage shutPage;
        private System.Windows.Forms.TabPage closePage;
        private System.Windows.Forms.ComboBox appList;
        private System.Windows.Forms.Button abortClose;
        private System.Windows.Forms.Button acceptClose;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown closingTimer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button searchService;
        private System.Windows.Forms.Button refreshList;
        private System.Windows.Forms.GroupBox notification;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button deleteService;
        private System.Windows.Forms.Button openLog;
    }
}

