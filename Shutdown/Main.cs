﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.ServiceProcess;
using System.Security.Principal;
using System.ComponentModel;
using System.Management;

namespace Shutdown
{
    public partial class Main : Form
    {
        const long max_value = 315359999;
        DateTime max_date;
        ServiceController sc;
        string log_path;
        public Main()
        {
            try
            {
                InitializeComponent();

                int k = DateTime.Now.Year;
                k += 10;
                int d = DateTime.Now.Day;
                if (!DateTime.IsLeapYear(k) && DateTime.Now.Month == 2 && d == 29)
                    d -= 3;
                else
                    d -= 2;
                
                max_date = new DateTime(k, DateTime.Now.Month, d, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                dateTimePicker.MaxDate = max_date;
                dateTimePicker.MinDate = DateTime.Now;

                setMinutes.Maximum = max_value/60;
                setMinutes.Minimum = 1;

                statusLabel.ForeColor = System.Drawing.Color.Black;
                statusLabel.Text = "Готов к работе.";
            }
            catch(Exception e)
            {
                StreamWriter sw = new StreamWriter("err.log", true);
                sw.WriteLine(DateTime.Now);
                sw.WriteLine(e.Message);
                sw.WriteLine(e.Source);
                sw.WriteLine(e.StackTrace);
                sw.Close();
            }
        }

        #region Shutdown
        private void Abort_Click(object sender, EventArgs e)
        {
            if(Start("/a")==0)
            {
                    statusLabel.ForeColor = System.Drawing.Color.Black;
                    statusLabel.Text = "Запланированное действие отменено.";
            }
        }

        int Start(string arg)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.System)+"\\shutdown.exe";
            Process pr = new Process();
            pr.StartInfo.FileName = path;
            pr.StartInfo.Arguments = arg;
            pr.StartInfo.CreateNoWindow = true;
            pr.StartInfo.UseShellExecute = false;
            pr.Start();
            Thread.Sleep(500);

            switch(pr.ExitCode)
            {
                case 0:
                    return 0;
                case 1:
                    {
                        statusLabel.Text = "Введено некоректное значение.";
                        statusLabel.ForeColor = System.Drawing.Color.Red;
                        break;
                    }
                case 1190:
                    {
                        statusLabel.Text = "Отмените предыдущее планирование.";
                        statusLabel.ForeColor = System.Drawing.Color.Red;
                        break;
                    }
                case 1116:
                    {
                        statusLabel.Text = "Ничего не запланировано.";
                        statusLabel.ForeColor = System.Drawing.Color.Red;
                        break;
                    }
                default:
                    {
                        statusLabel.Text = "Код " + pr.ExitCode + ". Непредусмотренная ошибка.";
                        statusLabel.ForeColor = System.Drawing.Color.Red;
                        break;
                    }
            }
            return pr.ExitCode;
        }

        private void Acept_Click(object sender, EventArgs e)
        {
            string arg="";
            if (shutdownRB.Checked)
                arg += "/s";
            else if (restartRB.Checked)
                arg += "/r";
            else
                arg += "/l";

            if(firstRB.Checked)
            {
                long setSec = (long)setMinutes.Value * 60;
                arg += " /t " + setSec.ToString();
            }
            else
            {
                DateTime dt = new DateTime(dateTimePicker.Value.Year,dateTimePicker.Value.Month,
                    dateTimePicker.Value.Day,(int)hourBox.SelectedItem,(int)minuteBox.Items[minuteBox.SelectedIndex],0);


                long setSec = (dt - DateTime.Now).Days*24*3600 + (dt - DateTime.Now).Hours*3600+ (dt - DateTime.Now).Minutes * 60 + (dt - DateTime.Now).Seconds+1;
                if (setSec > max_value)
                    setSec = max_value;
                arg += " /t " + setSec.ToString();
            }

            if (hardReboot.Checked)
                arg += " /f";
            if (Start(arg) == 0)
            {
                statusLabel.ForeColor = System.Drawing.Color.Black;
                statusLabel.Text = "Действие запланировано.";
            }
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            DateTimePicker dt = (DateTimePicker)sender;
            if (dt.Value.Date == DateTime.Now.Date)
            {
                hourBox.Items.Clear();
                minuteBox.Items.Clear();

                int count_minute = 0;
                int count_hour = DateTime.Now.Hour;
                if (DateTime.Now.Minute == 59)
                {
                    count_minute = 0;
                    count_hour++;
                }
                else
                    count_minute = DateTime.Now.Minute;

                for (int i = count_minute; i < 60; i++)
                {
                    minuteBox.Items.Add(i);
                }
                for(int i = count_hour; i<24;i++)
                {
                        hourBox.Items.Add(i);
                }
            }
            else if(dt.Value.Date == max_date.Date)
            {
                hourBox.Items.Clear();
                minuteBox.Items.Clear();
                for (int i = 0; i <= DateTime.Now.Minute; i++)
                {
                    if (i <= DateTime.Now.Hour)
                        hourBox.Items.Add(i);
                    minuteBox.Items.Add(i);
                }
            }
            else
            {
                hourBox.Items.Clear();
                minuteBox.Items.Clear();
                for (int i = 0; i < 60; i++)
                {
                    if (i < 24)
                        hourBox.Items.Add(i);
                    minuteBox.Items.Add(i);
                }
            }
            hourBox.SelectedIndex = 0;
            minuteBox.SelectedIndex = 0;
        }

        private void hourBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(dateTimePicker.Value.Date == DateTime.Now.Date && (int)hourBox.SelectedItem==DateTime.Now.Hour)
            {
                minuteBox.Items.Clear();
                for(int i=DateTime.Now.Minute+1;i<60;i++)
                {
                    minuteBox.Items.Add(i);
                }
            }
            else if (dateTimePicker.Value.Date==max_date.Date)
            {
                minuteBox.Items.Clear();
                for (int i = 0; i <= DateTime.Now.Minute; i++)
                {
                    minuteBox.Items.Add(i);
                }
            }
            else
            {
                minuteBox.Items.Clear();
                for (int i = 0; i < 60; i++)
                {
                    minuteBox.Items.Add(i);
                }
                minuteBox.SelectedIndex = 0;
            }
        }

        private void secondRB_CheckedChanged(object sender, EventArgs e)
        {
            bool flag = secondRB.Checked;
            dateTimePicker.Enabled = flag;
            hourBox.Enabled = flag;
            minuteBox.Enabled = flag;
        }

        private void firstRB_CheckedChanged(object sender, EventArgs e)
        {
            setMinutes.Enabled = firstRB.Checked;
        }
#endregion

        #region Service
        private void chooseAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            int si = chooseAction.SelectedIndex;
            if(si==1)
            {
                WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                bool isAdministrator = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
                if (isAdministrator == false)
                {
                    if (MessageBox.Show("Необходимо запустить программу от имени администратора. Запустить?", "Необходим перезапуск", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        ProcessStartInfo processInfo = new ProcessStartInfo();
                        processInfo.Verb = "runas";
                        processInfo.FileName = Application.ExecutablePath;
                        try
                        {
                            Process.Start(processInfo);
                        }
                        catch (Win32Exception ex)
                        {
                            MessageBox.Show(ex.Message,"Не удалось сменить привелегии программы",MessageBoxButtons.OK,MessageBoxIcon.Information);
                            chooseAction.SelectedIndex = 0;
                            return;
                        }
                        Application.Exit();
                    }
                    else
                    {
                        chooseAction.SelectedIndex = 0;
                        return;
                    }
                }
                log_path = Path.GetTempPath() + @"CApp.log";
                checkService();
            }
            else
            {
                statusLabel.ForeColor = System.Drawing.Color.Black;
                statusLabel.Text = "Готов к работе.";
            }
        }

        private void getApps()
        {
            appList.Items.Clear();
            Process[] processList = Process.GetProcesses();
            foreach(Process pr in processList)
            {
                if ((int)pr.MainWindowHandle > 0 && pr.MainWindowTitle != "")
                {
                    appList.Items.Add(pr.ProcessName);
                }
            }
        }

        private void acceptClose_Click(object sender, EventArgs e)
        {
            try
            {
                if(appList.SelectedIndex<0)
                {
                    statusLabel.ForeColor = System.Drawing.Color.Red;
                    statusLabel.Text = "Необходимо выбрать приложение.";
                    return;
                }
                if(Process.GetProcessesByName(appList.Items[appList.SelectedIndex].ToString()).GetLength(0) ==0)
                {
                    MessageBox.Show("Приложение не запущено.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    getApps();
                    return;
                }
                sc = new ServiceController("CApp");
                if (sc.Status == ServiceControllerStatus.Stopped)
                {
                    sc.Start(new string[] { appList.Items[appList.SelectedIndex].ToString(), closingTimer.Value.ToString(),log_path });

                    Thread.Sleep(1500);
                    sc.Refresh();
                    if(sc.Status==ServiceControllerStatus.Running)
                    {
                        statusLabel.ForeColor = System.Drawing.Color.Black;
                        statusLabel.Text = "Действие запланировано.";
                    }
                    else
                    {
                        statusLabel.ForeColor = System.Drawing.Color.Red;
                        statusLabel.Text = "Служба была отлючена. Действие отменено.";
                    }
                }
                else
                {
                    statusLabel.ForeColor = System.Drawing.Color.Red;
                    statusLabel.Text = "Действие уже запланировано.";
                }
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void abortClose_Click(object sender, EventArgs e)
        {
            sc.Refresh();
            if (sc==null || sc.Status!= ServiceControllerStatus.Running)
            {
                statusLabel.ForeColor = System.Drawing.Color.Red;
                statusLabel.Text = "Ничего не запланированно.";
            }
            else
            {
                sc.Stop();
                statusLabel.ForeColor = System.Drawing.Color.Black;
                statusLabel.Text = "Действие отменено.";
                sc.Refresh();
            }
        }

        private void refreshList_Click(object sender, EventArgs e)
        {
            getApps();
            statusLabel.ForeColor = System.Drawing.Color.Black;
            statusLabel.Text = "Список приложений обновлён.";
        }

        private void searchService_Click(object sender, EventArgs e)
        {
            checkService();
        }

        private void checkService()
        {
            bool isInstalled = false;
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController s in services)
            {
                if (s.ServiceName == "CApp")
                {
                    statusLabel.ForeColor = System.Drawing.Color.Black;
                    statusLabel.Text = "Служба найдена.";
                    isInstalled = true;
                    break;
                }
            }
            if (isInstalled)
            {
                getApps();
            }
            else
            {
                if (MessageBox.Show("Служба не установлена. Установить?", "Служба не найдена.", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    if (File.Exists(Application.StartupPath + "\\CloseApp Installer.msi"))
                    {
                        Process pr = new Process();
                        pr.StartInfo.FileName = "CloseApp Installer.msi";
                        pr.Start();
                        pr.WaitForExit();
                        if (pr.ExitCode == 0)
                        {
                            statusLabel.ForeColor = System.Drawing.Color.Black;
                            statusLabel.Text = "Служба установлена.";
                        }
                        else
                        {
                            statusLabel.ForeColor = System.Drawing.Color.Red;
                            statusLabel.Text = "Установка завершилась неудачей.";
                            return;
                        }
                        getApps();
                    }
                    else
                    {
                        statusLabel.ForeColor = System.Drawing.Color.Red;
                        statusLabel.Text = "Не удалось установить службу. Файл установки не обнаружен.";
                    }
                }
                else
                {
                    chooseAction.SelectedIndex = 0;
                    return;
                }
            }
        }

        private void deleteService_Click(object sender, EventArgs e)
        {
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController s in services)
            {
                if (s.ServiceName == "CApp")
                {
                    Process unistall = new Process();
                    unistall.StartInfo.Arguments = "/passive /x{5497ED2C-F0F6-4D1A-8C60-AC8F2BB5485F}";
                    unistall.StartInfo.FileName = "MsiExec.exe";
                    unistall.Start();
                    return;
                }
            }
            statusLabel.ForeColor = System.Drawing.Color.Red;
            statusLabel.Text = "Служба не установлена. Удаление невозможно.";
        }

        private void openLog_Click(object sender, EventArgs e)
        {
            if (File.Exists(log_path))
            {
                Process pr = new Process();
                pr.StartInfo.Arguments = log_path;
                pr.StartInfo.FileName = "notepad.exe";
                pr.Start();
            }
            else
            {
                statusLabel.ForeColor = System.Drawing.Color.Red;
                statusLabel.Text = "Лог-файл не найден.";
            }
        }
        #endregion

    }
}